#include <stdio.h>
#include <stdlib.h>
#include <string.h>

// Load the dictionary of words from the given filename.
// Return a pointer to the array of strings.
// Sets the value of size to be the number of valid
// entries in the array (not the total array length).
char ** loadDictionary(char *filename, int *size) {
	FILE *infile = fopen(filename, "r");
	if (!infile) { 
	    perror("Can't open dictionary");
	    exit(1);
	}
    int i = 0;
	char exp[100];
    *size = 0;
    while (fgets(exp, 100, infile)) {
        (*size)++;
    }
    fclose(infile);
	// TODO
	// Allocate memory for an array of strings (arr).
    char** arr = (char**) malloc(sizeof(char *) * (*size));
	// Read the dictionary line by line. 
  	infile = fopen(filename, "r");
    while (fgets(exp, 100, infile)) {
	    // Expand array if necessary (realloc).
        arr[i] = (char*) malloc(sizeof(char) * strlen(exp));
	    // Allocate memory for the string (str).
	    // Copy each line into the string (use strcpy).
        strcpy(arr[i], exp);
        i++;
    }
    fclose(infile);
	// Return pointer to the array of strings.
	return arr;
}

// Search the dictionary for the target string
// Return the found string or NULL if not found.
char * searchDictionary(char *target, char **dictionary, int size) {
    if (dictionary == NULL || target == NULL) return NULL;
    
	for (int i = 0; i < size; i++) {
	    if (strcmp(target, dictionary[i]) == 0) {
	        return dictionary[i];
	    }
	}
	return NULL;
}





